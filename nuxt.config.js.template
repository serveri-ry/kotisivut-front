
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  ////////mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*Remote debugging*/
  server: {     
    port: 8000, // default: 3000     
    host: '0.0.0.0', // default: localhost   
  },
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    //title: "Serveri ry" || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Itä-Suomen Yliopiston Kuopion kampuksen tietojenkäsittelytieteen opiskelijat' },
      { lang: 'fi' },
      { nativeUI: true },
      { name: 'msapplication-TileColor', content: '#0f5a62' },
      { hid: 'og:description', ogDescription: 'Itä-Suomen Yliopiston Kuopion kampuksen tietojenkäsittelytieteen opiskelijat' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    "~assets/animations.css"
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/pwa',
    'nuxt-compress',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/markdownit',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    [
      '@nuxtjs/component-cache',
      {
        max: 10000,
        maxAge: 1000 * 60 * 60
      }
    ],
    [
      "nuxt-compress",
      {
        gzip: {
          cache: true
        },
        brotli: {
          threshold: 10240
        }
      }
    ],
    // Sitemap has to be last
    '@nuxtjs/sitemap',
  ],
  sitemap: {
    //hostname: 'https://example.com',
    gzip: true,
    exclude: [
    ],
    routes: [
      {
        url: '/uutiset',
        changefreq: 'daily',
        priority: 1,
        //lastmod: ''
      },
      {
        url: '/jarjesto/tietojenkasittelytiede',
        changefreq: 'monthly',
        priority: 0.7,
        //lastmod: ''
      },
      {
        url: '/jarjesto/saannot',
        changefreq: 'yearly',
        priority: 0.3,
        //lastmod: ''
      },
      {
        url: '/jarjesto/hallitus',
        changefreq: 'monthly',
        priority: 0.5,
        //lastmod: ''
      },
      {
        url: '/jarjesto/koodiluola',
        changefreq: 'monthly',
        priority: 0.2,
        //lastmod: ''
      },
      {
        url: '/jarjesto',
        changefreq: 'monthly',
        priority: 0.8,
        //lastmod: ''
      },
      {
        url: '/connect',
        changefreq: 'monthly',
        priority: 0.5,
        //lastmod: ''
      },
      {
        url: '/english',
        changefreq: 'monthly',
        priority: 0.8,
        //lastmod: ''
      },
      {
        url: '/about',
        changefreq: 'yearly',
        priority: 0.1,
        //lastmod: ''
      }
    ]
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  pwa: {
    icon: {
      source: '~static/icon.png'
    },
    manifest: {
      name: 'Serveri ry',
      lang: 'fi',
      background_color: '#0f5a62'
    },
    meta: {
      theme_color: '#212121',
      mobileAppIOS: true,
      appleStatusBarStyle: 'black-translucent'
    }
  },
  loading: {
    color: 'white',
    height: '5px'
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  },
  markdownit: {
    preset: "default",
    linkify: true,
    breaks: true,
    injected: true,
    html: true,
  },
  //generate: { fallback: '404.html' },

}