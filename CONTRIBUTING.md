# Contributing
If you want to help editing something on the frontend, you need node and npm. Then just clone this frontend project, "npm install" and you are good to go! You can start the development server with "npm run dev". For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org). When your changes are ready to be merged, submit a pull request

While editing the website keep in mind:
1. Our goal is to be fast and mobile friendly
2. Avoid loading code from external sources, so fonts, scripts, icons and images must be available for us to host. 
4. All commits will be under the project licence, so before commit, check that you agree to the terms
3. Memes and easter eggs are allowed - if they respect everyone and are legal to use

