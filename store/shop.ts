import ShopService from "~/services/PostsService"

export const state = () => ({
  shop: [] as any[],
  error: null as any
})

export type ShopState = ReturnType<typeof state>

export const mutations = {
  SET_SPONSORS(state: any, responseshop: any) {
    state.shop = responseshop
  },
}

/*export default {
  getProducts() {
    return "kissa"
  }
}*/

export const getters = {
  // Searches for correct array index by id and returns that
  getShopIndex: (state: any) => (id: any) => {
    let i
    for (i = 0; i <= state.shop.length; i++) {
      console.log("beforeerror?: " + state.shop)
      try {
        if (state.shop[i].url == id) {
          return i
        }
      } catch {
        console.log("get shopindex failed")
        return 0
      }

      console.log("aftererror?")
    }
    // Implement not found error here!!
  },
  getShopId: (state: any) => (index: any) => {
    if (index <= -1 || index > state.shop.length) {
      console.log('requested index is out of range')
      return 'requested index out of range'
    }
    else {
      console.log('requested index is in range')
      if (state.shop[index] == 'undefined') {
        console.log("requested index doesn't exist")
        return 'id doesnt exist'
      }
      else {
        try {
          let id = state.shop[index]
          return id
        }
        catch {
          console.log('getshopid reading state.posts[index].postid failed')
          // Handle next post missing case
          return false
        }
      }
    }

    // implement not found
  }
}

// https://www.youtube.com/watch?v=NS0io3Z75GI
export const actions = {
  async loadPosts({ commit } : {commit:any}) {
    return ShopService.getPosts().then(response => {
      commit('SET_POSTS', response.data.posts)
    })
  }
}