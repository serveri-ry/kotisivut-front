import BoardService from "~/services/PostsService"

export const state = () => ({
  board: [] as any[],
  error: null as any
})

export type BoardState = ReturnType<typeof state>

export const mutations = {
  SET_SPONSORS(state: any, responseboard: any) {
    state.board = responseboard
  },
}

export const getters = {
  // Searches for correct array index by id and returns that
  getBoardIndex: (state: any) => (id: any) => {
    let i
    for (i = 0; i <= state.board.length; i++) {
      console.log("beforeerror?: " + state.board)
      try {
        if (state.board[i].url == id) {
          return i
        }
      } catch {
        console.log("get boardindex failed")
        return 0
      }

      console.log("aftererror?")
    }
    // Implement not found error here!!
  },
  getBoardId: (state: any) => (index: any) => {
    if (index <= -1 || index > state.board.length) {
      console.log('requested index is out of range')
      return 'requested index out of range'
    }
    else {
      console.log('requested index is in range')
      if (state.board[index] == 'undefined') {
        console.log("requested index doesn't exist")
        return 'id doesnt exist'
      }
      else {
        try {
          let id = state.board[index]
          return id
        }
        catch {
          console.log('getboardid reading state.posts[index].postid failed')
          // Handle next post missing case
          return false
        }
      }
    }

    // implement not found
  }
}

// https://www.youtube.com/watch?v=NS0io3Z75GI
export const actions = {
  async loadPosts({ commit } : {commit:any}) {
    return BoardService.getPosts().then(response => {
      commit('SET_POSTS', response.data.posts)
    })
  }
}