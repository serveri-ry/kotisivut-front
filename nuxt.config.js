export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  ////////mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  /*Remote debugging*/
  server: {
    port: 8000, // default: 3000
    host: "0.0.0.0", // default: localhost
  },
  serverMiddleware: [
    // Server-side redirects
    "~/serverMiddleware/redirects",
  ],
  publicRuntimeConfig: {
    gitSHA: process.env.NUXT_ENV_CURRENT_GIT_SHA || "commit"
  },
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    //title: "Serveri ry" || '',
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "Itä-Suomen Yliopiston Kuopion kampuksen tietojenkäsittelytieteen opiskelijat",
      },
      { lang: "fi" },
      { nativeUI: true },
      { name: "msapplication-TileColor", content: "#0f5a62" },
      {
        hid: "og:description",
        ogDescription:
          "Itä-Suomen Yliopiston Kuopion kampuksen tietojenkäsittelytieteen opiskelijat",
      },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/apple-touch-icon.png",
      },
      {
        rel: "alternate",
        type: "application/rss+xml",
        title: "Serverin uutiset ja tiedotteet opiskelijajäsenille",
        href: "https://rss.serveriry.fi/fi",
      },
    ],
    script: [
      {
        src: "https://js.stripe.com/v3",
        defer: true,
      },
      /*{
          src: "/underwater.js",
          defer: "true"
      }*/
    ],
  },
  /*
   ** Global CSS
   */
  css: ["~assets/animations.css", "~assets/fixtext.css"],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    //{src: "~/plugins/underwater.client.js"}
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    "@nuxt/typescript-build",
    "@nuxtjs/snipcart",
    "@nuxtjs/pwa",
    //"nuxt-compress",
  ],
  snipcart: {
    // Options available
    version: "3.2.1" /* not required default value is v3.0.23 */,
    key:
      "MWMyMGIxZTAtMWUwMC00YWIwLWIyMDItZTUwMDI1NGE1Mzk1NjM3Njk2NjU0NDMwOTY0NTAz" /* required https://app.snipcart.com/dashboard/account/credentials */,
    //attributes: /* not required default [] */,
    //locales: {} /* not required */
    defer: true,
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    "@nuxtjs/markdownit",
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/robots",
    /*'nuxt-i18n',*/
    "@nuxtjs/pwa",
    [
      "@nuxtjs/component-cache",
      {
        max: 10000,
        maxAge: 1000 * 60 * 60,
      },
    ],
    /*[
      "nuxt-compress",
      {
        gzip: {
          cache: true,
        },
        brotli: {
          threshold: 5000,
        },
      },
    ],*/
    "nuxt-protected-mailto",
    // Sitemap has to be last
    "@nuxtjs/sitemap",
  ],
  i18n: {
    locales: ["fi", "en"],
    defaultLocale: "fi",
  },
  sitemap: {
    hostname: "https://serveriry.fi",
    gzip: true,
    exclude: [
      "/ruoka",
      "/linkit",
      "/jarjesto/koodiluola",
      "/experimentalindex",
      "/media",
      "/jarjesto/tietojenkasittelytiede-vanha",
      "/jarjesto/liity/onnistui",
      "/jarjesto/hallitus/old",
      "/jarjesto/liity/checkout",
      "/areyousure/continue",
      "/shop",
      "/connect",
    ],
    routes: [
      {
        url: "/",
        changefreq: "monthly",
        priority: 1,
        lastmod: process.env.LASTMOD_INDEX,
      },
      {
        url: "/uutiset",
        changefreq: "daily",
        priority: 0.9,
        //lastmod: ''
      },
      {
        url: "/jarjesto/tietojenkasittelytiede",
        changefreq: "monthly",
        priority: 0.7,
        lastmod: process.env.LASTMOD_JARJ_TIETOJENK,
      },
      {
        url: "/jarjesto/saannot",
        changefreq: "yearly",
        priority: 0.3,
        lastmod: process.env.LASTMOD_JARJ_SAANNOT,
      },
      {
        url: "/jarjesto/hallitus",
        changefreq: "monthly",
        priority: 0.5,
        //lastmod: ''
      },
      {
        url: "/jarjesto/koodiluola",
        changefreq: "monthly",
        priority: 0.2,
        //lastmod: ''
      },
      {
        url: "/seuraa",
        changefreq: "monthly",
        priority: 0.5,
        lastmod: process.env.LASTMOD_SEURAA
      },
      {
        url: "/english",
        changefreq: "monthly",
        priority: 0.8,
        lastmod: process.env.LASTMOD_ENGLISH
      },
      {
        url: "/about",
        changefreq: "yearly",
        priority: 0.1,
        lastmod: process.env.LASTMOD_ABOUT
      },
      {
        url: "/tietosuoja",
        changefreq: "yearly",
        priority: 0.1,
        lastmod: process.env.LASTMOD_TIETOSUOJA
      },
    ],
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  pwa: {
    icon: {
      source: "~static/icon.png",
    },
    manifest: {
      name: "Serveri ry",
      lang: "fi",
      background_color: "#0f5a62",
    },
    meta: {
      theme_color: "#212121",
      mobileAppIOS: true,
      appleStatusBarStyle: "black-translucent",
    },
  },
  loading: {
    color: "white",
    height: "5px",
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    html: {
      minify: {
        decodeEntities: false,
      },
    },
  },
  markdownit: {
    preset: "default",
    linkify: true,
    breaks: true,
    injected: true,
    html: true,
  },
  //generate: { fallback: '404.html' },
  robots: [
    {
      UserAgent: "*",
      Disallow: "",
    },
    {
      Disallow: "/jarjesto/koodiluola",
    },
    {
      Disallow: "/experimentalindex",
    },
    {
      Disallow: "/jarjesto/tietojenkasittelytiede-vanha",
    },
    {
      Disallow: "/jarjesto/hallitus/old",
    },
    {
      Disallow: "/jarjesto/liity/onnistui",
    },
    {
      Disallow: "/jarjesto/koodiluola",
    },
    {
      Disallow: "/ruoka",
    },
    {
      Disallow: "/linkit",
    },
    {
      Disallow: "/areyousure",
    },
    {
      Disallow: "/shop",
    },
    {
      Sitemap: "https://serveriry.fi/sitemap.xml",
      CrawlDelay: 5,
    },
  ],
};
