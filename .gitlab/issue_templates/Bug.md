Write in either Finnish or English

1. Where the bug occured?
2. Steps to reproduce
3. (optional) Relevant screenshots
3. What browser are you using and if the issue is visual, add also details if it was mobile or desktop screen
4. what's the commit hash (from footer)